package com.navis.gatecontrol.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.navis.gatecontrol.model.TruckVisit;
import com.navis.gatecontrol.service.TruckVisitService;

/**
 * 
 * @author Felipe Nova
 * Class responsible for exposing the rest layer
 *
 */
@Controller
@RequestMapping("/rest")
public class TruckVisitController {

	private static final Logger logger = LoggerFactory.getLogger(TruckVisitController.class);

	@Autowired
	private TruckVisitService truckVisitService;
	
	private Gson gson = new Gson();
	
	/**
	 * Method responsible for receiving the request and call the method that will make the integration of data.
	 * @param truckVisit
	 * @param request
	 * @return ResponseEntity<String>
	 */
	@RequestMapping(value = "/gatein", method = RequestMethod.POST)
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	@Consumes(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> gateIn(@RequestBody TruckVisit truckVisit,HttpServletRequest request){	
		try{
			truckVisitService.gateIn(truckVisit);
			return new ResponseEntity<String>("GateIn realizado com sucesso.", HttpStatus.OK);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/gateout/{truckLicense}", method = RequestMethod.PUT)
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	@Consumes(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> gateOut(@PathVariable("truckLicense") String truckLicense,HttpServletRequest request){	
		try{
			truckVisitService.gateOut(truckLicense);
			return new ResponseEntity<String>("GateOut realizado com sucesso.", HttpStatus.OK);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/truckvisits", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getAllTruckVisits(HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			List<TruckVisit> list = truckVisitService.getAll();
			response = gson.toJson(list);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/truckvisits/{trucklicense}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getTruckVisitByTruckLicense(@PathVariable("trucklicense") String trucklicense,HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			List<TruckVisit> truckVisits = truckVisitService.searchTruckVisitByTruckLicense(trucklicense);
			response = gson.toJson(truckVisits);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	

}
