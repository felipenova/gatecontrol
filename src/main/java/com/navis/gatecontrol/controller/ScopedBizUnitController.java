package com.navis.gatecontrol.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.navis.gatecontrol.model.ScopedBizUnit;
import com.navis.gatecontrol.service.ScopedBizUnitService;

/**
 * 
 * @author Felipe Nova
 * Class responsible for exposing the rest layer
 *
 */
@Controller
@RequestMapping("/rest")
public class ScopedBizUnitController {

	private static final Logger logger = LoggerFactory.getLogger(ScopedBizUnitController.class);

	@Autowired
	private ScopedBizUnitService scopedBizUnitService; 
	
	private Gson gson = new Gson(); 
	
	@RequestMapping(value = "/scopedbizunits", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getAllScopedBizUnits(HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			List<ScopedBizUnit> list = scopedBizUnitService.getAll();
			response = gson.toJson(list);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/scopedbizunits/{gkey}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getScopedBizUnitByGkey(@PathVariable("gkey") Long gkey,HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			ScopedBizUnit bzu = scopedBizUnitService.getByGkey(gkey);
			response = gson.toJson(bzu);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	

	

}
