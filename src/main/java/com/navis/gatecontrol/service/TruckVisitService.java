package com.navis.gatecontrol.service;

import java.util.List;

import com.navis.gatecontrol.model.TruckVisit;

public interface TruckVisitService extends GenericService<TruckVisit> {
	public TruckVisit gateIn(TruckVisit truckVisit) throws Exception;
	public TruckVisit gateOut(String truckLicense) throws Exception;
	public List<TruckVisit> searchTruckVisitByTruckLicense(String search) throws Exception;
}
