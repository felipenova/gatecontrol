package com.navis.gatecontrol.service;


import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.navis.gatecontrol.dao.GenericDAO;
import com.navis.gatecontrol.dao.TruckVisitDAO;
import com.navis.gatecontrol.model.ScopedBizUnit;
import com.navis.gatecontrol.model.TruckVisit;
import com.navis.gatecontrol.truckvisitenum.TruckVisitStatusEnum;

/**
 * Class service responsible for application logic and call the DAO for database manipulation.
 * @author Felipe Nova
 *
 */
@Transactional(readOnly = true)
public class TruckVisitServiceImpl extends GenericServiceImpl<TruckVisit> implements TruckVisitService {

	private static final Logger logger = LoggerFactory.getLogger(TruckVisitServiceImpl.class);

	@Autowired
	private ScopedBizUnitService scopedBizUnitService;

	private TruckVisitDAO truckVisitDAO;


	public TruckVisitServiceImpl(){

	}

	@Autowired
	public TruckVisitServiceImpl(
			@Qualifier("truckVisitDAO") GenericDAO<TruckVisit> genericDao) {
		super(genericDao);
		this.truckVisitDAO = (TruckVisitDAO) genericDao;
	}

	public TruckVisit gateIn(TruckVisit truckVisit) throws Exception{
		try {
			TruckVisit tv = truckVisitDAO.searchTruckVisitByTruckLicenseAndStatusOk(truckVisit.getTvdtlsTruckLicenseNbr().trim());
			if(tv != null){
				throw new Exception("Esse caminh�o j� est� dentro do terminal.");
			}
			setRelationship(truckVisit);
			truckVisit.setTvdtlsCreated(new Date());
			truckVisit.setTvdtlsCreator("zilics");
			truckVisit.setTvdtlsEnteredYard(new Date());
			truckVisit.setTvdtlsTruckStatus(TruckVisitStatusEnum.OK);
			return truckVisitDAO.gateIn(truckVisit);
		} catch(Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public TruckVisit gateOut(String truckLicense) throws Exception {
		try {
			TruckVisit tv = truckVisitDAO.searchTruckVisitByTruckLicenseAndStatusOk(truckLicense.trim());
			if(tv == null){
				throw new Exception("Esse caminh�o n�o est� dentro do terminal.");
			}
			tv.setTvdtlsChanged(new Date());
			tv.setTvdtlsChanger("zilics");
			tv.setTvdtlsExitedYard(new Date());
			tv.setTvdtlsTruckStatus(TruckVisitStatusEnum.COMPLETE);
			return truckVisitDAO.gateOut(tv);
		} catch(Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	private void setRelationship(TruckVisit truckVisit) throws Exception{
		ScopedBizUnit scopedBizUnit = scopedBizUnitService.getByGkey(truckVisit.getTvdtlsTrkCompany().getBzuGkey());
		if(scopedBizUnit != null){
			truckVisit.setTvdtlsTrkCompany(scopedBizUnit);
		}
	}

	@Override
	public List<TruckVisit> searchTruckVisitByTruckLicense(String search) throws Exception {
		return truckVisitDAO.searchTruckVisitByTruckLicense(search);
	}





}
