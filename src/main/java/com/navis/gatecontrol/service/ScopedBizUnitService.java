package com.navis.gatecontrol.service;

import com.navis.gatecontrol.model.ScopedBizUnit;

public interface ScopedBizUnitService extends GenericService<ScopedBizUnit> {
}
