package com.navis.gatecontrol.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.navis.gatecontrol.truckvisitenum.BizRoleEnum;

@Entity
@Table(name="ScopedBizUnit") 
@NamedQueries(value={
		@NamedQuery(name="search.scopedbizunit.all", 
				query="select sb from ScopedBizUnit sb"),
		@NamedQuery(name="search.scopedbizunit.by.gkey", 
		query="select sb from ScopedBizUnit sb where sb.bzuGkey = :gkey")
})
public class ScopedBizUnit implements Serializable{

	private static final long serialVersionUID = -3527155578029983597L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bzuGkey;

	@NotNull
	@Size(max=255)
	@Column(name="bzuName",nullable=false,length=255,unique=true)
	private String bzuName;
	
	@NotNull
	@Column(name="bzuRole",nullable=false)
	@Enumerated(EnumType.STRING)
	private BizRoleEnum bzuRole;
	
	@NotNull
	@Size(max=50)
	@Column(name="bzuId",nullable=false,length=50,unique=true)
	private String bzuId;
	
	public ScopedBizUnit(){}

	public Long getBzuGkey() {
		return bzuGkey;
	}

	public void setBzuGkey(Long bzuGkey) {
		this.bzuGkey = bzuGkey;
	}

	public String getBzuName() {
		return bzuName;
	}

	public void setBzuName(String bzuName) {
		this.bzuName = bzuName;
	}

	public BizRoleEnum getBzuRole() {
		return bzuRole;
	}

	public void setBzuRole(BizRoleEnum bzuRole) {
		this.bzuRole = bzuRole;
	}

	public String getBzuId() {
		return bzuId;
	}

	public void setBzuId(String bzuId) {
		this.bzuId = bzuId;
	}
	
}
