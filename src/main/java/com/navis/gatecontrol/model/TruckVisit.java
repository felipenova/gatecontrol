package com.navis.gatecontrol.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.navis.gatecontrol.truckvisitenum.TruckVisitStatusEnum;

@Entity
@Table(name="TruckVisit") 
@NamedQueries(value={
		@NamedQuery(name="search.truckvisit.all", 
		query="select tv from TruckVisit tv"),
		@NamedQuery(name="search.truckvisit.by.gkey", 
		query="select tv from TruckVisit tv where tv.cvdGkey = :gkey"),
		@NamedQuery(name="search.truckvisit.by.trucklicense", 
		query="select tv from TruckVisit tv where upper(tv.tvdtlsTruckLicenseNbr) = :trucklicense"),
		@NamedQuery(name="search.truckvisit.by.trucklicense.and.status.ok", 
		query="select tv from TruckVisit tv where upper(tv.tvdtlsTruckLicenseNbr) = :trucklicense and tv.tvdtlsTruckStatus = :status")
})
public class TruckVisit implements Serializable{

	private static final long serialVersionUID = 1391677794646280340L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cvdGkey;

	@NotNull
	@Size(max=7)
	@Column(name="tvdtlsTruckLicenseNbr",nullable=false,length=7)
	private String tvdtlsTruckLicenseNbr;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "bzuGkey")
	private ScopedBizUnit tvdtlsTrkCompany;

	@NotNull
	@Column(name="tvdtlsTruckStatus",nullable=false)
	@Enumerated(EnumType.STRING)
	private TruckVisitStatusEnum tvdtlsTruckStatus;
	
	@NotNull
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="tvdtlsEnteredYard",nullable=false)
	private Date tvdtlsEnteredYard;
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="tvdtlsExitedYard",nullable=true)
	private Date tvdtlsExitedYard;
	
	@NotNull
	@Size(max=50)
	@Column(name="tvdtlsDriverName",nullable=false,length=50)
	private String tvdtlsDriverName;
	
	@NotNull
	@Size(max=25)
	@Column(name="tvdtlsDriverLicenseNbr",nullable=false,length=25)
	private String tvdtlsDriverLicenseNbr;
	
	@NotNull
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="tvdtlsCreated",nullable=false)
	private Date tvdtlsCreated;

	@NotNull
	@Size(max=50)
	@Column(name="tvdtlsCreator",nullable=false,length=50)
	private String tvdtlsCreator;

	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="tvdtlsChanged",nullable=true)
	private Date tvdtlsChanged;

	@Size(max=50)
	@Column(name="tvdtlsChanger",nullable=true,length=50)
	private String tvdtlsChanger;

	

	public TruckVisit(){}



	public Long getCvdGkey() {
		return cvdGkey;
	}



	public void setCvdGkey(Long cvdGkey) {
		this.cvdGkey = cvdGkey;
	}



	public String getTvdtlsTruckLicenseNbr() {
		return tvdtlsTruckLicenseNbr;
	}



	public void setTvdtlsTruckLicenseNbr(String tvdtlsTruckLicenseNbr) {
		this.tvdtlsTruckLicenseNbr = tvdtlsTruckLicenseNbr;
	}

	public ScopedBizUnit getTvdtlsTrkCompany() {
		return tvdtlsTrkCompany;
	}



	public void setTvdtlsTrkCompany(ScopedBizUnit tvdtlsTrkCompany) {
		this.tvdtlsTrkCompany = tvdtlsTrkCompany;
	}



	public TruckVisitStatusEnum getTvdtlsTruckStatus() {
		return tvdtlsTruckStatus;
	}



	public void setTvdtlsTruckStatus(TruckVisitStatusEnum tvdtlsTruckStatus) {
		this.tvdtlsTruckStatus = tvdtlsTruckStatus;
	}



	public Date getTvdtlsEnteredYard() {
		return tvdtlsEnteredYard;
	}



	public void setTvdtlsEnteredYard(Date tvdtlsEnteredYard) {
		this.tvdtlsEnteredYard = tvdtlsEnteredYard;
	}



	public Date getTvdtlsExitedYard() {
		return tvdtlsExitedYard;
	}



	public void setTvdtlsExitedYard(Date tvdtlsExitedYard) {
		this.tvdtlsExitedYard = tvdtlsExitedYard;
	}



	public String getTvdtlsDriverName() {
		return tvdtlsDriverName;
	}



	public void setTvdtlsDriverName(String tvdtlsDriverName) {
		this.tvdtlsDriverName = tvdtlsDriverName;
	}



	public String getTvdtlsDriverLicenseNbr() {
		return tvdtlsDriverLicenseNbr;
	}



	public void setTvdtlsDriverLicenseNbr(String tvdtlsDriverLicenseNbr) {
		this.tvdtlsDriverLicenseNbr = tvdtlsDriverLicenseNbr;
	}



	public Date getTvdtlsCreated() {
		return tvdtlsCreated;
	}



	public void setTvdtlsCreated(Date tvdtlsCreated) {
		this.tvdtlsCreated = tvdtlsCreated;
	}



	public String getTvdtlsCreator() {
		return tvdtlsCreator;
	}



	public void setTvdtlsCreator(String tvdtlsCreator) {
		this.tvdtlsCreator = tvdtlsCreator;
	}



	public Date getTvdtlsChanged() {
		return tvdtlsChanged;
	}



	public void setTvdtlsChanged(Date tvdtlsChanged) {
		this.tvdtlsChanged = tvdtlsChanged;
	}



	public String getTvdtlsChanger() {
		return tvdtlsChanger;
	}



	public void setTvdtlsChanger(String tvdtlsChanger) {
		this.tvdtlsChanger = tvdtlsChanger;
	}


}
