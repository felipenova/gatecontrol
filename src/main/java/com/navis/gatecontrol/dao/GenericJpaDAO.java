package com.navis.gatecontrol.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

/**
 * Class responsible for database manipulation RefCountry.
 * @author Felipe Nova
 *
 */
@Repository
public class GenericJpaDAO<ENT> implements GenericDAO<ENT> {
	@PersistenceContext(unitName="gatecontrol_DataSource", name="gatecontrol_DataSource")
	protected EntityManager manager;

	protected Class<ENT> persistentClass;
	
	@SuppressWarnings("unchecked")
	protected GenericJpaDAO(){
		Type type = getClass().getGenericSuperclass();
        while (!(type instanceof ParameterizedType) || ((ParameterizedType) type).getRawType() != GenericJpaDAO.class) {
            if (type instanceof ParameterizedType) {
                type = ((Class<?>) ((ParameterizedType) type).getRawType()).getGenericSuperclass();
            } else {
                type = ((Class<?>) type).getGenericSuperclass();
            }
        }
        this.persistentClass = (Class<ENT>) ((ParameterizedType) type).getActualTypeArguments()[0];
	}

	public List<ENT> getAll() throws Exception{
		String namedQuery = "search."+persistentClass.getSimpleName().toLowerCase()+".all";
		TypedQuery<ENT> q = manager.createNamedQuery(namedQuery, persistentClass);
		return q.getResultList();
	}

	public ENT getByGkey(Long gkey) throws Exception{
		try{
			String namedQuery = "search."+persistentClass.getSimpleName().toLowerCase()+".by.gkey";
			TypedQuery<ENT> q = manager.createNamedQuery(namedQuery, persistentClass);
			q.setParameter("gkey", gkey);
			return q.getSingleResult();
		}catch(NoResultException e){
			throw new NoResultException("N�o foi encontrado um "+ persistentClass.getSimpleName() +" com o gkey especificado");
		}
	}


}
