package com.navis.gatecontrol.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.navis.gatecontrol.model.TruckVisit;
import com.navis.gatecontrol.truckvisitenum.TruckVisitStatusEnum;

/**
 * Class responsible for database manipulation.
 * @author Felipe Nova
 *
 */
@Repository
public class TruckVisitJpaDAO extends GenericJpaDAO<TruckVisit> implements TruckVisitDAO {

	@Override
	public TruckVisit gateIn(TruckVisit truckVisit) throws Exception {
		manager.persist(truckVisit);
		manager.flush();
		return truckVisit;
	}

	@Override
	public TruckVisit gateOut(TruckVisit truckVisit) throws Exception {
		TruckVisit tv = manager.merge(truckVisit);
		manager.flush();
		return tv;
	}

	@Override
	public List<TruckVisit> searchTruckVisitByTruckLicense(String search) throws Exception {
		String namedQuery = "search.truckvisit.by.trucklicense";
		TypedQuery<TruckVisit> q = manager.createNamedQuery(namedQuery, TruckVisit.class);
		q.setParameter("trucklicense", search);
		return q.getResultList();
	}

	@Override
	public TruckVisit searchTruckVisitByTruckLicenseAndStatusOk(String search) throws Exception {
		try{
			String namedQuery = "search.truckvisit.by.trucklicense.and.status.ok";
			TypedQuery<TruckVisit> q = manager.createNamedQuery(namedQuery, TruckVisit.class);
			q.setParameter("trucklicense", search);
			q.setParameter("status", TruckVisitStatusEnum.OK);
			return q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}

}
