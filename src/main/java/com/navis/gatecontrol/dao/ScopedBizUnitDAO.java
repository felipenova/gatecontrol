package com.navis.gatecontrol.dao;

import com.navis.gatecontrol.model.ScopedBizUnit;

public interface ScopedBizUnitDAO extends GenericDAO<ScopedBizUnit> {
	
}
