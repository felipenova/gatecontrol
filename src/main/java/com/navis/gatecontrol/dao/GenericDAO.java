package com.navis.gatecontrol.dao;

import java.util.List;

public interface GenericDAO<ENT> {
	public List<ENT> getAll() throws Exception;
	public ENT getByGkey(Long gkey) throws Exception;
}
