package com.navis.gatecontrol.dao;

import java.util.List;

import com.navis.gatecontrol.model.TruckVisit;

public interface TruckVisitDAO extends GenericDAO<TruckVisit> {
	public TruckVisit gateIn(TruckVisit truckVisit) throws Exception;
	public TruckVisit gateOut(TruckVisit truckVisit) throws Exception;
	public List<TruckVisit> searchTruckVisitByTruckLicense(String search) throws Exception;
	public TruckVisit searchTruckVisitByTruckLicenseAndStatusOk(String search) throws Exception;
}
